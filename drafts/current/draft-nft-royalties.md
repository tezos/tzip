---
title: NFT Royalties
status: Draft
author: Josh Dechant (@codecrafting)
type: Interface
created: 2022-08-09
extends: tzip-012
date: 2023-01-06
version: 3
---

## Table of Contents

- [Summary](#summary)
- [Abstract](#abstract)
- [Motivation](#motivation)
- [Specification](#specification)
- [Rationale](#rationale)
- [Backwards Compatibility](#backwards-compatibility)
- [Security Considerations](#security-considerations)
- [Implementations](#implementations)
- [Copyright](#copyright)

## Summary

This proposal describes a standardized way to retrieve royalty payment information
for non-fungible tokens (NFTs) to enable universal support for royalty payments
across all NFT marketplaces and ecosystem participants.

## Abstract

This standard allows contracts, such as NFTs that support [TZIP-012][1] interface,
to signal a royalty amount to be paid to the NFT creator
or rights holder every time the NFT is sold or re-sold. This is intended for NFT
marketplaces that want to support the ongoing funding of artists and other NFT
creators. The royalty payment must be voluntary, as transfers do not always imply
a sale occurred. Marketplaces and individuals implement this standard by
retrieving the royalty payment information with an on chain view,
which specifies how much to pay to which address for a given sale price. The exact
mechanism for paying and notifying the recipient is out of the scope of this standard.
This standard should be considered a minimal building block for further innovation
in NFT royalty payments.

## Motivation

There are many marketplaces for NFTs. This standard enables all marketplaces to
retrieve royalty payment information for a given NFT. This enables accurate royalty
payments regardless of which marketplace the NFT is sold or re-sold at.

This standard specifies royalty information retrieval that can be accepted across
any type of NFT marketplace. This minimalist proposal only provides a mechanism
to fetch the royalty amount and recipient(s). The actual funds transfer is something
which the marketplace should execute.

This standard allows NFTs that support [TZIP-012][1] interface,
to have a standardized way of signalling royalty information. More specifically, these
contracts can now calculate a royalty amount to provide to the rightful recipient(s).

Royalty amounts are always a percentage of the sale price. If a marketplace chooses
not to implement this standard, then no funds will be paid for secondary sales. It is
believed that the NFT marketplace ecosystem will voluntarily implement this royalty
payment standard; in a bid to provide ongoing funding for artists/creators. NFT
buyers will assess the royalty payment as a factor when making NFT purchasing
decisions.

Without an agreed royalty payment standard, the NFT ecosystem will lack an
effective means to collect royalties across all marketplaces and artists and other
creators will not receive ongoing funding. This will hamper the growth and adoption
of NFTs and demotivate NFT creators from minting new and innovative tokens.

Enabling all NFT marketplaces to unify on a single royalty payment standard will
benefit the entire NFT ecosystem.

While this standard focuses on NFTs and compatibility with the FA2 ([TZIP-012][1])
standard, it does not require compatibility with [TZIP-012][1]. Any other contract could
integrate this standard to return royalty payment information. This is, therefore, a
universal royalty standard for many asset types.

## Specification

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [RFC 2119][3].

Contracts **MAY** implement this standard for royalties to provide a
standard method of specifying royalty payment information.

Marketplaces that support this standard **SHOULD** implement some method of
transferring royalties to the royalty recipient(s).

Marketplaces that support this standard **MUST** pay royalties for sales that they
facilitate, no matter where the sale occurred or in what currency, including (but 
not limited to) on-chain sales, auctions, lotteries, over-the-counter (OTC) trades,
and other off-chain equivalents. As royalty payments are voluntary, entities that
respect this standard **MUST** pay no matter where the sale occurred - a sale
conducted outside of the blockchain is still a sale. The exact mechanism for paying
and notifying the recipient is out of the scope of this standard.

Marketplaces that support this standard **SHOULD NOT** send a zero-value
transaction if the royalty amount(s) is 0. This serves no useful purpose in
this standard.

### On Chain Views

Contracts implementing this standard **MUST** have the following [**Michelson**][4] view: `get_royalties`

```
view "get_royalties"
     (nat %token_id)
     (pair (nat %total_shares) (map %shares address nat))
```

Shares represents a percentage in the form `%share / %total_shares`. For
example, consider the following CameLIGO data structure:

```
{
  total_shares = 10_000n;
  shares = Map.literal [
    (("tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP" : address), 500n);
    (("tz1ZQYMDETodNBAc2XVbhZFGme8KniuPqrSw" : address), 750n)]
}
```

The above data respresents a royalty of 5% to
tz1QLne6uZFxPRdRfJG8msx5RouENpJoRsfP (`500 / 10_000`) and 7.5% to
tz1ZQYMDETodNBAc2XVbhZFGme8KniuPqrSw (`750 / 10_000`).

Invocations of `get_royalties` view **MUST NOT** return a cumulative shares
value greater than `%total_shares`.

Subsequent invocations of `get_royalties` **MAY** return different values.

Subsequent invocations of `get_royalties` **SHOULD NOT** return values based
on variables that are unpredictable.

Implementers of this standard **MUST NOT** make assumptions on the unit of
exchange.

Marketplaces **MUST** pay the royalty in the same unit of exchange as that
of the sale. For example, if the sale price is in XTZ, then the royalty
payment must also be paid in XTZ, and if the sale price is in USDT,
then the royalty payment must also be paid in USDT.

Marketplaces **SHOULD** calculate a percentage of the sale price
when calculating the royalty amount using the formula:

`royalties = price * share / total_shares`

If the royalty fee calculation results in a remainder, marketplaces **MAY**
round up or round down to the nearest natural number. For example, if the
royalty fee is 10% and the sale price is 999, both 99 and 100 are valid
royalty amounts.

### Global Constants

## calc_royalties

This standard provides a global constant to calculate royalties due at:

`expru2hTf7Qe5QVb7UWdLKVedax3Snw3NyewjDX3aQYUQfD9xCR6YY`

Assuming the following Ligo data types (CameLIGO):

```
type royalty_shares = (address, nat) map

type royalties =
  [@layout:comb] {
  total_shares  : nat;
  shares        : royalty_shares
}

type calc_royalties_params =
  [@layout:comb] {
  price         : nat;
  royalties     : royalties
}
```

The global constant has the following signature:

```
calc_royalties : calc_royalties_params -> royalty_shares
```

And was generated with the following CameLIGO code:

```
let calc_royalties (params : calc_royalties_params) : royalty_shares =
  if params.royalties.total_shares > 0n
  then
    let sum_shares : nat = Map.fold
      (fun (a, share : nat * (address * nat)) : nat -> a + share.1) params.royalties.shares 0n
    in
    let () = assert_with_error (sum_shares <= params.royalties.total_shares)
      "INVALID_SHARE_VALUES"
    in
    Map.map
      (fun (_, share : address * nat) ->
        params.price * share / params.royalties.total_shares) params.royalties.shares
  else
    Map.empty
```

Marketplaces implementing this standard are **RECOMMENDED** to use this
global constant to perform the royalty calculations.


## Rationale

### Optional royalty payments

It is impossible to know which NFT transfers are the result of sales, and which are
not. Therefore, we cannot force every transfer function to involve a royalty payment
as not every transfer is a sale that would require such payment. We believe the NFT
marketplace ecosystem will voluntarily implement this royalty payment standard to
provide ongoing funding for artists/creators. NFT buyers will assess the royalty
payment as a factor when making NFT purchasing decisions.

### Royalty payment percentage calculation

This standard mandates a percentage-based royalty fee model. It is likely that the
most common case of percentage calculation will be where the royalty amount(s)
are always calculated from the sale price using a fixed percent i.e. if the royalty fee
is 10%, then a 10% royalty fee must apply whether the sale price is 10, 10000 or 1234567890.

### Royalties for existing tokens and contracts

A royalty registry standard should be developed to allow for existing tokens to
utilize this standard. Creation of such a royalty registry is out of the scope
of this standard.

### Unit-less royalty payment across all marketplaces, both on-chain and off-chain

This standard does not specify a currency or token used for sales and royalty payments.
The same percentage-based royalty fee(s) must be paid regardless of what currency,
or token was used in the sale, paid in the same currency or token. This applies to
sales in any location including on-chain sales, over-the-counter (OTC) sales, and
off-chain sales using fiat currency such as at auction houses. As royalty payments
are voluntary, entities that respect this standard must pay no matter where the sale
occurred - a sale outside of the blockchain is still a sale. The exact mechanism for
paying and notifying the recipient is out of the scope of this standard.

### Universal Royalty Payments

Although designed specifically with NFTs in mind, this standard does not require that
a contract implementing this standard is compatible with the FA2 ([TZIP-012][1])
token standard. Any contract could use this interface to return royalty payment
information, provided that it is able to uniquely identify assets within the constraints
of the interface. This is, therefore, a universal royalty standard for many asset types.

## Backwards Compatibility

This standard is compatible with current [TZIP-012][1] standards.

## Security Considerations

Marketplaces and other consumers of the `get_royalties` view should take care to validate
the return values. In particular, they should validate that the sum of all shares is not
greater than `%total_shares`. The provided global constant does this validation.

## Implementations

A sample implementation in a CameLIGO:

```
type token_id = nat

type royalty_shares = (address, nat) map

type royalties =
  [@layout:comb] {
  total_shares  : nat;
  shares        : royalty_shares
}

type storage = {
  royalties     : (token_id, royalties) big_map
}

[@view]
let get_royalties (token_id, s : token_id * storage) : royalties =
  match Big_map.find_opt token_id s.royalties with
    Some royalties -> royalties
  | None -> { total_shares = 0n; shares = Map.empty }
```

Implementing the `calc_royalties` Global Constant in PascaLIGO:

```
type royalty_shares = (address, nat) map

type royalties =
  [@layout:comb] {
  total_shares  : nat;
  shares        : royalty_shares
}

type calc_royalties_params =
  [@layout:comb] {
  price         : nat;
  royalties     : royalties
}

let calc_royalties (p : calc_royalties_params) : royalty_shares =
  (Tezos.constant "expru2hTf7Qe5QVb7UWdLKVedax3Snw3NyewjDX3aQYUQfD9xCR6YY")(p)
```

## Copyright

Copyright and related rights waived via [CC0][5].


[1]: https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-12/tzip-12.md
[2]: https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-21/tzip-21.md
[3]: https://www.ietf.org/rfc/rfc2119.txt
[4]: https://docs.tezos.com/smart-contracts/languages/michelson
[5]: https://www.ietf.org/rfc/rfc2413.txt
