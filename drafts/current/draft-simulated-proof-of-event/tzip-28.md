---
title: Simulated Proof of Event
status: Draft
authors: Abhishek Jain <abhishek.jain@trili.tech>
type: LA
created: 2023-06-18
date: 2023-06-18
version: 1
---

## Table of Contents

- [Summary](#summary)
- [Motivation](#motivation)
- [Specification](#specification)
    - [Request](#request)
    - [Response](#response)
    - [Operations List Structure](#operations-list-structure)
    - [Verification](#verification)
- [Sequence Diagram](#sequence-diagram)
- [User Flow](#user-flow)
- [Copyright](#copyright)

## Summary

This proposal is an extension of TZIP-27 and it describes a standard for simulating proof of event emission without requiring on-chain transaction. In this proposed standard, the wallet application can communicate a list of operations that if they were to be executed, would result in a desired proof of event emission. The idea is it should avoid the need for gas fees, block finality and verification of on-chain data.

## Motivation

Proof of Event has some challenges: (i) it requires onchain event emission, (ii) gas fees, and (iii) state management for both the wallet applications and dApps.

While it works well for complex abstracted accounts which may require internal management and coordination to emit the requested proof of event, it may seem costly and burdensome for simpler usecases of abstracted accounts. For example, a single implicit account controlled smart contract wallet. Users of such wallets would be more sensitive to user friction and gas costs which may stack up.

**Simulated Proof of Event** works on the principle that if a user, claiming authority of a specific abstracted account, demonstrates ability to emit the requisite Proof of Event then it would be sufficient evidence of ownership and no onchain Proof of Event emission would be necessary.

This approach is designed with ease and user experience in mind to reduce burden and make it comparable in mechanics to standard message signing.

## Specification

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119][1].

A new `verification_type` is to be introduced, named  `simulated_proof_of_event`. The wallet application **MUST** share the list of supported verification types through this field separated by vertical bars (`|`). For example, `proof_of_event|simulated_proof_of_event`. The dApp is **REQUIRED** to pick only one of those verification options as it sees fit. It is **RECOMMENDED** that preference based on left to right ordering of the `verification_type`.

### Request

For Beacon and WalletConnect 2.0, introduce a new request object for simulated proof of event challenges (to use the same internal structure as for Proof of Event),
```typescript
// Beacon
export interface SimulatedProofOfEventChallengeRequest extends BeaconBaseMessage {
    type: BeaconMessageType.SimulatedProofOfEventChallengeRequest;
    payload: string;
    contractAddress: string;
}
```
```json
// WalletConnect 2.0
{
   ...
   "method": "tezos_simulatedProofOfEventChallenge",
   "params": {
       "payload": $payload,
       "contractAddress": $contractAddress,
   }
}
```
- `payload`: payload for Proof of Event
- `contractAddress`: the contract address of the abstracted account

### Response

The Wallet application, in response to the dApp's challenge, must communicate an ordered list of operations, which the dApp can independantly verify to emit a specific Proof of Event if they were to be executed onchain.

To reduce user friction and back and forth, unlike Proof of Event, Wallet Applications wouldn't be required to accept or reject the challenge since there isn't any state being recorded on either side. Instead the Wallet Application can only respond with a challenge response.

For Beacon and WalletConnect2.0, introduce a new responses for simulated proof of event  as follows,
```typescript
// Beacon
export interface SimulatedProofOfEventChallengeResponse extends BeaconBaseMessage {
 type: BeaconMessageType.SimulatedProofOfEventChallengeResponse;
 operationsList: string; // Base64 encoded json
 errorMessage: string;
}
```
```json
// WalletConnect 2.0
{
   ...
   "method": "tezos_simulatedProofOfEventChallengeResponse",
   "param":  {
       "operationsList": $operationListJson, // json
       "errorMessage": $error_message,
   }
}
```
- `operationsList`: The operations list is an ordered list of operations that are to be simulated and verified by the dApp to observe for the required Proof of Event emission. Specifications below.

### Operations List Structure

It is **RECOMMENDED** that the wallet application should set the `fees` to zero to prevent unintended injection of the operations.

The json structure for the Operation List JSON is as follows:

```json
[
    "contents": $operation_group
    "branch": $branch,
    "signature": $signature,
    ...
]
```
- `contents`: json structure for operations
- `branch`: the block hash of the branch
- `signature`: base58 format of the signature of the operation

### Verification

dApps are at liberty to decide the method of verification. Essentially, dApps need to pre-apply the list of operations and observe the emitted events and verify atleast one matches the requested Proof of Event.

It is **RECOMMENDED** for simplicity and convinience that the dApp can query through there choice of RPC Node. The RPC endpoint `POST ../<block_id>/helpers/preapply/operations` can be utilized to perform the simulation.

**Note**:
1. The value of RPC endpoint is exactly the same format as the one communicated from the wallet application in response to the challenge.
1. Due to the design of the Tezos Protocol, if the implicit account has a balance of 0 XTZ, the simulation will naturally fail. Additionally, a revelation is necessary before the dApp proceeds with the simulation operations.

## Sequence Diagram

![Sequence Diagram](sequence_diagram.png "Sequence Diagram")

## User Flow

Below User flow demonstrates the how an abstracted account supporting wallet application shares an signed operation which would emit a Proof of Event to verification to the dApp.

![User Flow Demo](user_flow_demo.png "User Flow Demo")

## Copyright

Copyright and related rights waived via [CC0][2].


[1]: https://www.ietf.org/rfc/rfc2119.txt
[2]: https://www.ietf.org/rfc/rfc2413.txt
